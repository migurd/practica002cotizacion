
package Controlador;
import Modelo.Cotizacion;
import Vista.dlgCotizacion;
// Event listeners
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
// Views
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author quier
 */
public class Controlador implements ActionListener {

    private Cotizacion coti;
    private dlgCotizacion vista;
    
    public Controlador(Cotizacion coti, dlgCotizacion vista) {
        this.coti = coti;
        this.vista = vista;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        
    }
    
    public static void main(String[] args) {
        Cotizacion coti = new Cotizacion();
        dlgCotizacion vista = new dlgCotizacion(new JFrame(), true);
        
        Controlador contra = new Controlador(coti, vista);
        contra.iniciarVista();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource() == vista.btnNuevo) {
            limpiar();
            
            vista.txtCodigo.setEnabled(true);
            vista.txtDescripcion.setEnabled(true);
            vista.txtPrecio.setEnabled(true);
            vista.txtPorcentajePagoInicial.setEnabled(true);
            vista.txtPlazoPago.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(false);
        }
        
        if(e.getSource() == vista.btnGuardar) {
            // Save data and validations
            try {
                if(0 > Integer.parseInt(vista.txtCodigo.getText())) {
                    throw new IllegalArgumentException("El código no puede ser negativo");
                }
                else if(0 > Float.parseFloat(vista.txtPrecio.getText())) {
                    throw new IllegalArgumentException("El precio no puede ser negativo");
                }
                else if(Integer.parseInt(vista.txtPorcentajePagoInicial.getText()) > 100 || 0 > Integer.parseInt(vista.txtPorcentajePagoInicial.getText())) {
                    throw new IllegalArgumentException("El porcentaje no puede ser mayor de 100, ni menor a 0");
                }
                coti.setCodigo(Integer.parseInt(vista.txtCodigo.getText()));
                coti.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                coti.setPorcentajePagoInicial(Integer.parseInt(vista.txtPorcentajePagoInicial.getText()));
            }
            catch(NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
                return;
            }
            catch(IllegalArgumentException ex2) {
                JOptionPane.showMessageDialog(vista, ex2.getMessage());
                return;
            }
            catch(Exception ex3) {
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex3.getMessage());
                return;
            }
            
            try {
                if(vista.txtDescripcion.getText().isBlank())
                    throw new IllegalArgumentException("El texto de descripción no puede estar vacío");
                else
                    coti.setDescripcion(vista.txtDescripcion.getText());
                    
                switch(vista.txtPlazoPago.getSelectedIndex()) {
                    case 0:
                        // Create something to prevent error
                        throw new IllegalArgumentException("Seleciona una opción válida...");
                    case 1:
                        coti.setPlazoPago(3);
                        break;
                    case 2:
                        coti.setPlazoPago(6);
                        break;
                    case 3:
                        coti.setPlazoPago(12);
                        break;
                    case 4:
                        coti.setPlazoPago(24);
                        break;
                    case 5:
                        coti.setPlazoPago(36);
                        break;
                    case 6:
                        coti.setPlazoPago(48);
                        break;
                    case 7:
                        coti.setPlazoPago(72);
                        break;
                    default:
                        System.out.println("HOW DO YOU BREAK A COMBOBOX");
                }
                JOptionPane.showMessageDialog(vista, "Se guardó la información correctamente");
            }
            catch(IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(vista, ex.getMessage());
                return;
            }
            
            // Unlocks mostrar
            vista.btnMostrar.setEnabled(true);
            
            // Cleans so you can press mostrar
            limpiar();
            
        }
        
        if(e.getSource() == vista.btnMostrar) {
            
            vista.txtCodigo.setText(Integer.toString(coti.getCodigo()));
            vista.txtDescripcion.setText(coti.getDescripcion());
            vista.txtPrecio.setText(Float.toString(coti.getPrecio()));
            vista.txtPorcentajePagoInicial.setText(Integer.toString(coti.getPorcentajePagoInicial()));
            
            switch(coti.getPlazoPago()) {
                case 3:
                    vista.txtPlazoPago.setSelectedIndex(1);
                    break;
                case 6:
                    vista.txtPlazoPago.setSelectedIndex(2);
                    break;
                case 12:
                    vista.txtPlazoPago.setSelectedIndex(3);
                    break;
                case 24:
                    vista.txtPlazoPago.setSelectedIndex(4);
                    break;
                case 36:
                    vista.txtPlazoPago.setSelectedIndex(5);
                    break;
                case 48:
                    vista.txtPlazoPago.setSelectedIndex(6);
                    break;
                case 72:
                    vista.txtPlazoPago.setSelectedIndex(7);
                    break;
                default:
                    System.out.println("HOW DO YOU BREAK A COMBOBOX");
            }
            
            vista.txtPagoInicial.setText(Float.toString(coti.calcularPagoInicial()));
            vista.txtTotalFinanciar.setText(Float.toString(coti.calcularTotalFinanciar()));
            vista.txtPagoMensual.setText(Float.toString(coti.calcularPagoMensual()));
        }
        
        if(e.getSource() == vista.btnLimpiar) {
            limpiar();
        }
        
        if(e.getSource() == vista.btnCancelar) {
            limpiar();
            vista.txtCodigo.setEnabled(false);
            vista.txtDescripcion.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            vista.txtPorcentajePagoInicial.setEnabled(false);
            vista.txtPlazoPago.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
        }
        
        if(e.getSource() == vista.btnCerrar) {
            int option = JOptionPane.showConfirmDialog(vista, "¿Estás seguro que quieres salir?", "Confirmación para salir", JOptionPane.YES_NO_OPTION);
            if(option == JOptionPane.YES_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }
        
    }

    private void iniciarVista() {
        vista.setTitle("Cotización");
        vista.setSize(500, 600);
        vista.setVisible(true);
    }
    
    public void limpiar() {
        vista.txtCodigo.setText("");
        vista.txtDescripcion.setText("");
        vista.txtPrecio.setText("");
        vista.txtPorcentajePagoInicial.setText("");
        vista.txtPlazoPago.setSelectedIndex(0);

        vista.txtPagoInicial.setText("");
        vista.txtTotalFinanciar.setText("");
        vista.txtPagoMensual.setText("");
    }
    
}
